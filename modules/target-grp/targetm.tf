resource "aws_lb_target_group" "tg" {
  name     = var.tg
  port     = var.port
  protocol = "HTTP"
  vpc_id = var.vpc-id
  }