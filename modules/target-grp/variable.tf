variable "tg" {
  type = string
}
variable "port" {
  type = number
}
variable "vpc-id" {
  type = string
}
output "target-id" {
  value = aws_lb_target_group.tg.id
}