variable "rds-subnet-group-name" {
  type = string
}
variable "subnet-id" {
  type = list(string)
}
variable "project" {
  type = string
  default = "indigo"
}
variable "client" {
  type = string
  default = "indigo"
}
variable "environment" {
  type = string
  default = "prod"
}
variable "cluster-pg-name" {
  type = string
}
variable "db-pg-name" {
  type = string
}
variable "writer-identifier" {
  type = string
}
variable "reader-identifier" {
  type = string
}
variable "reader-size" {
  type = string
}
variable "writer-size" {
  type = string
}
variable "cluster-identifier" {
  type = string
}
variable "security-group" {
  type = list(string)
}

variable "admin" {
  type = string
  default = "indigo_user_prod"
}
variable "password" {
  type = string
  default = "hadna&H3afg"
}
