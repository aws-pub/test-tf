resource "aws_db_subnet_group" "subnet-id" {
  name       = var.rds-subnet-group-name
  subnet_ids = var.subnet-id

  tags = {
    Name = var.rds-subnet-group-name
    Project = var.project
    Client  = var.client
    Environment = var.environment
  }
}
/*
resource "aws_db_option_group" "example" {
  name                     = "${ar.cluster-identifier}-option-group"
  option_group_description = "indigo prod Option Group"
  engine_name              = "mysql"
  major_engine_version     = "8.0"

  option {
    option_name = "Timezone"

    option_settings {
      name  = "TIME_ZONE"
      value = "UTC"
    }
  }

  option {
    option_name = "SQLSERVER_BACKUP_RESTORE"

    option_settings {
      name  = "IAM_ROLE_ARN"
      value = aws_iam_role.example.arn
    }
  }

  option {
    option_name = "TDE"
  }
}
*/

resource "aws_rds_cluster_parameter_group" "cluster-pg" {
  name        = var.cluster-pg-name
  family      = "aurora-mysql5.7"
  description = "RDS default cluster parameter group"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}
resource "aws_db_parameter_group" "travel-db-pg" {
  name   = var.db-pg-name
  family = "aurora-mysql5.7"

  #parameter {
   ##value = "1"
  #}

  lifecycle {
    create_before_destroy = false
  }
}
resource "aws_rds_cluster_instance" "travel-instance" {
  #count              = 1
  #identifier         = "aurora-cluster-demo-${count.index}"
  identifier         =  var.writer-identifier
  cluster_identifier = aws_rds_cluster.rds-cluster.id
  instance_class     = var.writer-size
  publicly_accessible = false
  db_parameter_group_name = var.db-pg-name
  #db_parameter_group_name = "ver-ccc-cluster-uat-pg"
  engine             = aws_rds_cluster.rds-cluster.engine
  engine_version     = aws_rds_cluster.rds-cluster.engine_version
}

resource "aws_rds_cluster_instance" "instance-reader" {
  #count              = 1
  #identifier         = "aurora-cluster-demo-${count.index}"
  availability_zone = "us-east-1e"
  identifier         =  var.reader-identifier
  cluster_identifier = aws_rds_cluster.rds-cluster.id
  instance_class     = var.reader-size
  publicly_accessible = false
  db_parameter_group_name = var.db-pg-name
  #db_parameter_group_name = "ver-ccc-cluster-uat-pg"
  engine             = aws_rds_cluster.rds-cluster.engine
  engine_version     = aws_rds_cluster.rds-cluster.engine_version
}


resource "aws_rds_cluster" "rds-cluster" {
  cluster_identifier      = var.cluster-identifier
  #availability_zones      = ["us-east-1a", "us-east-1b"]
  #database_name           = "test"
  port                   = "3360"
  master_username        = var.admin
  master_password        = var.password
  #replication_source_identifier = "$[aws_rds_cluster_instance.travel-instance-reader.identifier]"
  db_cluster_parameter_group_name  = var.cluster-pg-name
  vpc_security_group_ids     = var.security-group
  db_subnet_group_name      = aws_db_subnet_group.subnet-id.name
  engine_version          = "5.7.mysql_aurora.2.11.2"
  engine                  = "aurora-mysql"
  skip_final_snapshot     = true
}