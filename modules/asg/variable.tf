########### TAG

variable "client" {
  type = string
}
variable "project" {
  type = string
}
variable "environment" {
  type = string
}

variable "asg-1" {
  type = string
}
variable "template-1" {
  type = string
}
variable "instance-type" {
  type = string
}
variable "security" {
  type = list(string)
}
variable "subnet" {
  type = list(string)
}
variable "ami" {
  type = string
}
#variable "target" {
#  type = list(string)
#}
variable "key" {
  type = string
}
variable "tag" {
  type = string
}
/*
variable "sh" {
  type = string
  default = filebase64("${path.module}/simplica-web.sh")
}
*/
variable "sh" {
  type = string
}