resource "aws_launch_template" "template-1" {
  name = var.template-1

 # block_device_mappings {
  #  device_name = "/dev/sda1"
#
 #   ebs {
  #    volume_size = 20
   # }
  #}
 # iam_instance_profile {
  #  name = "test"
  #}

  image_id = var.ami
  instance_type = var.instance-type
  key_name = var.key
   
   #iam_instance_profile {
   # name = "indigo-b2c-preprod-deploy-role"
  #}
  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
    instance_metadata_tags      = "enabled"
  }

  monitoring {
    enabled = true
  }

 # network_interfaces {
  #  associate_public_ip_address = false
  #}
  vpc_security_group_ids = var.security
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = var.template-1
      Client = "indigo"
      Project = "indigo"
      Environment = "preprod"
    }
  }

  user_data = var.sh 

}


resource "aws_autoscaling_group" "asg-1" {
  
  name                 = var.asg-1
  vpc_zone_identifier  = var.subnet
  #target_group_arns    = var.target
  health_check_type    = "ELB"
  launch_template {
    id      = aws_launch_template.template-1.id
    version = "$Latest"
  }

  min_size = 0
  max_size = 0

  tag {
    key                 = "Name"
    value               = var.asg-1
    propagate_at_launch = true
  }
  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = true
  }
  tag {
    key                 = "Client"
    value               = var.client
    propagate_at_launch = true
  }
  tag {
    key                 = "Project"
    value               = var.project
    propagate_at_launch = true
  }
}

