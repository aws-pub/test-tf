variable "redis-pg-name" {
  type = string
}
variable "family" {
  type = string
}
variable "redis-subnet-grp-name" {
  type = string
}
variable "redis-subnet-id" {
  type = list(string)
}
variable "redis-cluster-name" {
  type = string
}
variable "node-type" {
  type = string
}
variable "security-grp" {
  type = list(string)
}