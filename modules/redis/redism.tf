resource "aws_elasticache_parameter_group" "default" {
  name   = var.redis-pg-name
  family = var.family
}
resource "aws_elasticache_subnet_group" "subnet-group" {
  name       = var.redis-subnet-grp-name
  subnet_ids = var.redis-subnet-id
}

resource "aws_elasticache_cluster" "example" {
  cluster_id           = var.redis-cluster-name
  engine               = "redis"
  engine_version       =  "6.x"
  node_type            = var.node-type
  num_cache_nodes      = 1
  parameter_group_name = var.redis-pg-name
  security_group_ids = var.security-grp
  port                 = 6379
  subnet_group_name    = aws_elasticache_subnet_group.subnet-group.name
}