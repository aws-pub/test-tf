# Environment variable
variable "region" {}
variable "environment" {}
variable "project-name" {}

# Vpc variabel
variable "vpc_cidr" {}
variable "public-subnet-az1" {}
variable "public-subnet-az2" {}
variable "private_app_subnet_az1" {}
variable "private_app_subnet_az2" {}
variable "private_app_subnet_az3" {}
variable "private_app_subnet_az4" {}
variable "private_app_subnet_az5" {}
variable "private_app_subnet_az6" {}