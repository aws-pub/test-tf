output "vpc-id" {
    value = aws_vpc.vpc.id
}
output "public-subnet-1" {
    value = aws_subnet.public_subnet_az1.id
}
output "public-subnet-2" {
    value = aws_subnet.public_subnet_az2.id
}
