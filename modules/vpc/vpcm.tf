# create vpc
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.project-name}-${var.environment}-vpc"
  }
}

# create internet gateway and attach it to vpc
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project-name}-${var.environment}-igw"
  }
}

# use data source to get all avalablility zones in region
data "aws_availability_zones" "available_zones" {}

# create public subnet az1
resource "aws_subnet" "public_subnet_az1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public-subnet-az1
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project-name}-${var.environment}-public-subnet-1-ap-south-1a"
  }
}

# create public subnet az2
resource "aws_subnet" "public_subnet_az2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public-subnet-az2
  availability_zone       = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project-name}-${var.environment}-public-subnet-2-ap-south-1b"
  }
}

# create route table and add public route
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "${var.project-name}-${var.environment}-public-rt"
  }
}

# associate public subnet az1 to "public route table"
resource "aws_route_table_association" "public_subnet_az1_rt_association" {
  subnet_id      = aws_subnet.public_subnet_az1.id
  route_table_id = aws_route_table.public_route_table.id
}

# associate public subnet az2 to "public route table"
resource "aws_route_table_association" "public_subnet_2_rt_association" {
  subnet_id      = aws_subnet.public_subnet_az2.id
  route_table_id = aws_route_table.public_route_table.id
}

/*
# Nat gateway
resource "aws_nat_gateway" "Nat" {
  connectivity_type = "public"
  allocation_id     = aws_eip.nat-ip.id
  subnet_id         = aws_subnet.public_subnet_az1.id
  tags = {
    Name        = "${var.project-name}-${var.environment}-nat"
    Environment = var.environment
    Project     = var.project-name
  }
}
*/
resource "aws_eip" "nat-ip" {
  depends_on = [ aws_internet_gateway.internet_gateway ]
}
resource "aws_nat_gateway" "Nat" {
  subnet_id     = aws_subnet.public_subnet_az1.id
  allocation_id     = aws_eip.nat-ip.id

  tags = {
    Name        = "${var.project-name}-${var.environment}-nat"
    Environment = var.environment
    Project     = var.project-name
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.internet_gateway]
}
# create route table and add private route
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.Nat.id
  }

  tags = {
    Name = "${var.project-name}-${var.environment}-private-rt"
  }
}



# create private app subnet az1
resource "aws_subnet" "private_app_subnet_az1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_app_subnet_az1
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-${var.environment}-private-subnet-1-ap-south-1a"
  }
}

# create private app subnet az2
resource "aws_subnet" "private_app_subnet_az2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_app_subnet_az2
  availability_zone       = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-${var.environment}-private-subnet-2-ap-south-1b"
  }
}

# create private app subnet az3
resource "aws_subnet" "private_app_subnet_az3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_app_subnet_az3
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-${var.environment}-private-subnet-3-ap-south-1a"
  }
}

# create private app subnet az4
resource "aws_subnet" "private_app_subnet_az4" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_app_subnet_az4
  availability_zone       = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-${var.environment}-private-subnet-4-ap-south-1b"
  }
}

# create private app subnet az5
resource "aws_subnet" "private_app_subnet_az5" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_app_subnet_az5
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-${var.environment}-private-subnet-5-ap-south-1a"
  }
}

# create private app subnet az6
resource "aws_subnet" "private_app_subnet_az6" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_app_subnet_az6
  availability_zone       = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.project-name}-${var.environment}-private-subnet-6-ap-south-1b"
  }
}

# associate priavte subnet az1 to "private route table"
resource "aws_route_table_association" "private_subnet_az1_rt_association" {
  subnet_id      = aws_subnet.private_app_subnet_az1.id
  route_table_id = aws_route_table.private_route_table.id
}

# associate private subnet az2 to "private route table"
resource "aws_route_table_association" "private_subnet_2_rt_association" {
  subnet_id      = aws_subnet.private_app_subnet_az2.id
  route_table_id = aws_route_table.private_route_table.id
}

# associate private subnet az3 to "private route table"
resource "aws_route_table_association" "private_subnet_3_rt_association" {
  subnet_id      = aws_subnet.private_app_subnet_az3.id
  route_table_id = aws_route_table.private_route_table.id
}

# associate private subnet az4 to "private route table"
resource "aws_route_table_association" "private_subnet_4_rt_association" {
  subnet_id      = aws_subnet.private_app_subnet_az4.id
  route_table_id = aws_route_table.private_route_table.id
}

# associate private subnet az5 to "private route table"
resource "aws_route_table_association" "private_subnet_5_rt_association" {
  subnet_id      = aws_subnet.private_app_subnet_az5.id
  route_table_id = aws_route_table.private_route_table.id
}

# associate private subnet az6 to "private route table"
resource "aws_route_table_association" "private_subnet_6_rt_association" {
  subnet_id      = aws_subnet.private_app_subnet_az6.id
  route_table_id = aws_route_table.private_route_table.id
}