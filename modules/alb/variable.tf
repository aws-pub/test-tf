variable "name" {
  type = string
}
variable "lb-type" {
  type = string
}
variable "subnet" {
    type = list(string)
}
variable "vpc" {
    type = string
  
}
variable "security" {
  type = list(string)
}