resource "aws_lb" "test-lb" {
  name               = var.name
  internal           = false
  load_balancer_type = var.lb-type
  #security_groups    = [data.aws_security_group.security_grp-1.id]
  security_groups = var.security
  #subnets = data.aws_subnet_ids.subnets.ids
  subnets = var.subnet
  enable_deletion_protection = false
  }
 resource "aws_lb_listener" "travel-listner" {
  load_balancer_arn = aws_lb.test-lb.arn
  port              = "80"
  protocol          = "HTTP"
  #ssl_policy        = "ELBSecurityPolicy-2016-08"
  #certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
  
 }
 /*
  resource "aws_lb_listener" "travel-listner-1" {
  load_balancer_arn = aws_lb.test-lb.arn
  port              = "443"
  protocol          = "HTTPS"
  #ssl_policy        = "ELBSecurityPolicy-TLS13-1-2-2021-06"
  #certificate_arn   = "arn:aws:acm:ap-south-1:832827957392:certificate/14e5c8db-c688-4522-bff5-488a852ec066"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404 not found"
      status_code  = "200"
  }
  }
  
}
*/