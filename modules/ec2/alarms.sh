

#!/bin/bash
cd /home/ec2-user/alarm
unzip AmazonCloudWatchAgent.zip
./install.sh
cat << EOF >> config.json
{
	"metrics": {
	        "namespace": "EC2-mem&disk",
		"append_dimensions": {
			"InstanceId": "${aws:InstanceId}"
		},
		"metrics_collected": {
			"mem": {
				"measurement": [
					"mem_used_percent"
				],
				"metrics_collection_interval": 60
			},
            "disk": {
				"measurement": [
                     "disk_used_percent"
				],
				"metrics_collection_interval": 60
			}
		}
	}
}
EOF
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/home/ec2-user/alarm/config.json
