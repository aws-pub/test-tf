resource "aws_instance" "web" {
  availability_zone = var.zone
  instance_type = var.size
  associate_public_ip_address = true
  metadata_options{
  http_tokens = "required"
  http_endpoint = "enabled"
  }
  subnet_id = var.subnet
  iam_instance_profile = "terraform-test"
  vpc_security_group_ids = var.sg
  #key_name = "${aws_key_pair.deployer.key_name}"
  key_name = var.key
  #associate_public_ip_address = True
  ami = var.ami
  #ebs_block_device{
   # delete_on_termination = true
    #device_name = "/dev/sdc"
    #volume_size = 50
  #}
  root_block_device{
  volume_size = var.volume_size
  }
  
  tags = {
    Name = var.name
    Client = var.client
    Environment = var.env
    Project = var.project
    Snapshot = "true"
  }
  provisioner "remote-exec" {
  inline = [ "mkdir alarm" ]
}
  provisioner "file" {
  source = "/home/anuj/Documents/Vernost/terraform/indigo-b2c-prod/ec2/AmazonCloudWatchAgent.zip"
  destination = "/home/ec2-user/alarm/AmazonCloudWatchAgent.zip"
}
 provisioner "file" {
  source = "/home/anuj/Documents/Vernost/terraform/indigo-b2c-prod/ec2/alarm.sh"
  destination = "/home/ec2-user/alarm/alarm.sh"
}
provisioner "file" {
  source = "/home/anuj/Documents/Vernost/terraform/indigo-b2c-prod/ec2/config.json"
  destination = "/home/ec2-user/alarm/config.json"
}
provisioner "remote-exec" {
  inline = [ 
    "cd /home/ec2-user/alarm/",
    "chmod u+x alarm.sh",
    "sudo ./alarm.sh"
   ]
}
connection {
    type = "ssh"
    user = "ec2-user"
    private_key = file("${path.module}/indigo-b2c-pp-asg.pem")
    host = "${aws_instance.web.public_ip}"
  }
}
/*
resource "aws_iam_role_policy" "dlm_lifecycle" {
  name   = "dlm-lifecycle-policy"
  role   = aws_iam_role.dlm_lifecycle_role.id
  policy = data.aws_iam_policy_document.dlm_lifecycle.json
}
*/

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["dlm.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "dlm_lifecycle_role" {
  name               = "dlm-lifecycle-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "backup-policy" {
  statement {

    effect = "Allow"

            actions = [
                "ec2:CreateSnapshot",
                "ec2:CreateSnapshots",
                "ec2:DeleteSnapshot",
                "ec2:DescribeInstances",
                "ec2:DescribeVolumes",
                "ec2:DescribeSnapshots",
                "ec2:EnableFastSnapshotRestores",
                "ec2:DescribeFastSnapshotRestores",
                "ec2:DisableFastSnapshotRestores",
                "ec2:CopySnapshot",
                "ec2:ModifySnapshotAttribute",
                "ec2:DescribeSnapshotAttribute",
                "ec2:DescribeSnapshotTierStatus",
                "ec2:ModifySnapshotTier",
            ]

            resources = ["*"]
        }
        statement {
            effect = "Allow"
            actions = ["ec2:CreateTags"]
            resources = ["arn:aws:ec2:*::snapshot/*"]
        }
        statement {
            effect= "Allow"
            actions = [
                "events:PutRule",
                "events:DeleteRule",
                "events:DescribeRule",
                "events:EnableRule",
                "events:DisableRule",
                "events:ListTargetsByRule",
                "events:PutTargets",
                "events:RemoveTargets"
            ]
            resources = ["*"]
        }
}

resource "aws_iam_role_policy" "dlm_lifecycle" {
  name   = "dlm-lifecycle-policy"
  role   = aws_iam_role.dlm_lifecycle_role.id
  policy = data.aws_iam_policy_document.backup-policy.json
}

resource "aws_dlm_lifecycle_policy" "example" {
  description        = "example DLM lifecycle policy"
  execution_role_arn = aws_iam_role.dlm_lifecycle_role.arn
  state              = "ENABLED"
  tags = {
    Name = "test-policy"
  }
  

  policy_details {
    resource_types = ["INSTANCE"]

    schedule {
      name = "2 weeks of daily snapshots"

      create_rule {
        interval      = 1
        interval_unit = "HOURS"
        times         = ["11:30"]
      }

      retain_rule {
        count = 5
      }

      tags_to_add = {
        SnapshotCreator = "backup"
      }

      copy_tags = false
    }

    target_tags = {
      Snapshot = "true"
    }
  }
}

#Alarm

resource "aws_cloudwatch_metric_alarm" "alarm-cpu-80" {
  alarm_name          = "indigo-b2c-prod-${var.name}-CPUUtilization>=80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 120
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    InstanceId = aws_instance.web.id
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-cpu-85" {
  alarm_name          = "indigo-b2c-prod-${var.name}-CPUUtilization>=85"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 120
  statistic           = "Average"
  threshold           = 85

  dimensions = {
    InstanceId = aws_instance.web.id
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-cpu-90" {
  alarm_name          = "indigo-b2c-prod-${var.name}-CPUUtilization>=90"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 120
  statistic           = "Average"
  threshold           = 90

  dimensions = {
    InstanceId = aws_instance.web.id
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}

resource "aws_cloudwatch_metric_alarm" "alarm-mem-80" {
  alarm_name          = "${var.name}-MemoryUtilization>=80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "mem_used_percent"
  namespace           = "EC2-mem&disk"
  period              = 120
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    InstanceId = aws_instance.web.id
  }

  alarm_description = "This metric monitors ec2 memory utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-mem-85" {
  alarm_name          = "${var.name}-MemoryUtilization>=85"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "mem_used_percent"
  namespace           = "EC2-mem&disk"
  period              = 120
  statistic           = "Average"
  threshold           = 85

  dimensions = {
    InstanceId = aws_instance.web.id
  }

  alarm_description = "This metric monitors ec2 memory utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-mem-90" {
  alarm_name          = "${var.name}-MemoryUtilization>=90"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "mem_used_percent"
  namespace           = "EC2-mem&disk"
  period              = 120
  statistic           = "Average"
  threshold           = 90

  dimensions = {
    InstanceId = aws_instance.web.id
  }

  alarm_description = "This metric monitors ec2 memory utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-disk-80" {
  alarm_name          = "${var.name}-DiskUtilization>=80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "disk_used_percent"
  namespace           = "EC2-mem&disk"
  period              = 120
  statistic           = "Average"
  threshold           = 80
  dimensions = {
    path       = "/"
    InstanceId = aws_instance.web.id
    device = "xvda1"
    fstype      = "xfs"

  }
  alarm_description = "This metric monitors ec2 disk utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-disk-85" {
  alarm_name          = "${var.name}-DiskUtilization>=85"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "disk_used_percent"
  namespace           = "EC2-mem&disk"
  period              = 120
  statistic           = "Average"
  threshold           = 85
  dimensions = {
    path       = "/"
    InstanceId = aws_instance.web.id
    device = "xvda1"
    fstype      = "xfs"

  }
  alarm_description = "This metric monitors ec2 disk utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}
resource "aws_cloudwatch_metric_alarm" "alarm-disk-90" {
  alarm_name          = "${var.name}-DiskUtilization>=90"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "disk_used_percent"
  namespace           = "EC2-mem&disk"
  period              = 120
  statistic           = "Average"
  threshold           = 90
  #alarm_actions       = aws_sns_topic.default.*.arn
  dimensions = {
    path       = "/"
    InstanceId = aws_instance.web.id
    device = "xvda1"
    fstype      = "xfs"

  }
  alarm_description = "This metric monitors ec2 disk utilization"
  #alarm_actions     = [aws_autoscaling_policy.bat.arn]
}