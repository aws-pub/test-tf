variable "zone" {
  type = string
}
variable "size" {
  type = string
}
variable "ami" {
  type = string
}
variable "subnet" {
  type = string
}
variable "sg" {
  type = list(string)
}
variable "volume_size" {
  type = number
}
variable "key" {
    type = string
  
}
variable "name" {
  type = string
}
variable "client" {
  type = string
}
variable "project" {
  type = string
}
variable "env" {
  type = string
}