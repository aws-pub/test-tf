resource "aws_security_group" "security_grp-1" {
  name        = var.sg-name
  description = "Allow TLS inbound traffic"
  vpc_id      = var.vpc-id
  tags = {
    Name = var.tag
    Client = var.client
    Project = var.project
    Environment = var.env
  }

  dynamic "ingress" {

    for_each = var.ports
    iterator = ports
    content {
        from_port         = ports.value
        to_port           = ports.value
        protocol          = "tcp"
        cidr_blocks       = ["0.0.0.0/0"]

      
    }
    
  } 

 egress{
  from_port         = 0
  to_port           = 0
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
 }
}
