variable "sg-name" {
  type = string
}
variable "vpc-id" {
  type = string
}
variable "tag" {
  type = string
}
variable "client" {
  type = string
}
variable "project" {
  type = string
}
variable "env" {
  type = string
}
variable "ports" {
  type = list(number)
}
output "security-id" {
  value = aws_security_group.security_grp-1.id
}