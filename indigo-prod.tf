terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = "us-east-1"
  access_key = "AKIA6LRRAVHRG4MQ24ZN"
  secret_key = "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"
}
terraform {
  backend "s3" {
    bucket  = "indigo-prod-test-tf"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    #encrypt = "true"
    #dynamodb_table = "hands-on-cloud-terraform-remote-state-dynamodb"
    
  }
}
data "aws_vpc" "vpc" {
  id = "vpc-0f25387e150f84345"
}
data "aws_subnet" "Asg-subnet" {
  id = "subnet-02874831dd3df966a"
}
data "aws_subnet" "Asg-subnet-1" {
  id = "subnet-0da9fa1911370ad0b"
}
data "aws_security_group" "security_grp-1"{
   id = "sg-0bae8a36005762d08"
}



#### ec2 standalone servers


resource "aws_key_pair" "deployer" {
  key_name   = "indigo-b2c-pp-asg"
  public_key = file("${path.module}/indigo-b2c-pp-asg.pub")
}

 

module "server1" {
  
  #source = "./ec2/"
  source =  "git::https://gitlab.com/aws-pub/test-tf.git//modules/ec2"
  zone = "us-east-1b"
  size = "t2.micro"
  ami = "ami-0230bd60aa48260c6"
  subnet = "subnet-02874831dd3df966a"
  sg = ["sg-0bae8a36005762d08"]
  volume_size = "10"
  key = "indigo-b2c-pp-asg"
  name = "server-1"
  client = "indigo"
  project = "indigo"
  env = "prod"
}

/*
# redis instance

module "redis" {
  source = "./redis"
  redis-cluster-name = "indigo-b2c-prod-redis"
  redis-pg-name = "indigo-b2c-prod-redis-pg"
  redis-subnet-grp-name = "indigo-b2c-prod-redis-subnet"
  redis-subnet-id = [data.aws_subnet.Asg-subnet.id,data.aws_subnet.Asg-subnet-1.id]
  family = "redis6.x"
  security-grp = [data.aws_security_group.security_grp-1.id]
  node-type = "cache.t3.micro"
}


# rds and parameter groups

module "rds" {
  source = "./rds"
  rds-subnet-group-name = "indigo-b2c-prod-rds-subnet-group"
  subnet-id = [data.aws_subnet.Asg-subnet.id , data.aws_subnet.Asg-subnet-1.id]
  cluster-pg-name = "indigo-b2c-prod-rds-cluster-pg"
  db-pg-name = "indigo-b2c-prod-rds-pg"
  writer-identifier = "indigo-b2c-prod-rds"
  reader-identifier = "indigo-b2c-prod-rds-reader"
  cluster-identifier = "indigo-b2c-prod-rds-cluster"
  reader-size = "db.t3.medium"
  writer-size = "db.t3.medium"
  security-group = [data.aws_security_group.security_grp-1.id]

}

# vpc

module "vpc" {
  source = "./vpc/"
  region = "us-east-1"
  environment = "prod"
  project-name = "indigo-b2c"
  vpc_cidr = "192.168.224.0/21"
  public-subnet-az1 = "192.168.224.0/24"
  public-subnet-az2 = "192.168.225.0/24"
  private_app_subnet_az1 = "192.168.226.0/24"
  private_app_subnet_az2 = "192.168.227.0/24"
  private_app_subnet_az3 = "192.168.228.0/24"
  private_app_subnet_az4 = "192.168.229.0/24"
  private_app_subnet_az5 = "192.168.230.0/24"
  private_app_subnet_az6 = "192.168.231.0/24"
}

#security group

module "security" {
  source = "./sg"
  sg-name = "test"
  ports = [80,22]
  tag = "test-1"
  client = "test"
  project = "test"
  env = "test"
  vpc-id = module.vpc.vpc-id

}

#alb
module "alb" {
  source  = "./alb/"

  name = "indigo-b2c-preprod-common-alb"
  lb-type = "application"

  vpc           = module.vpc.vpc-id
  subnet            = [module.vpc.public-subnet-1, module.vpc.public-subnet-2]
  security   = [module.security.security-id]

}
#data "aws_lb_listener" "listener-443" {
#  arn = "arn:aws:elasticloadbalancing:ap-south-1:832827957392:listener/app/indigo-b2c-preprod-common-alb/b94f787a86800f3b/a261e86cb6398cc9"
#}



#target-group

module "target-group" {
  source = "./target-grp"
  tg = "test"
  port = 8080
  vpc-id = module.vpc.vpc-id
}

# ASG

resource "aws_key_pair" "deployer" {
  key_name   = "indigo-b2c-pp-asg"
  public_key = file("${path.module}/indigo-b2c-pp-asg.pub")
}

 module "asg" {
  source ="./asg/"
  asg-1 = "test"
  key = "indigo-b2c-pp-asg"
  instance-type = "t2.micro"
  #security = [module.security.security-id]
  #subnet = [module.vpc.public-subnet-1]
  security = [data.aws_security_group.security_grp-1.id]
  subnet = [data.aws_subnet.Asg-subnet.id]
  template-1 = "test-1"
  ami = "ami-0230bd60aa48260c6"
  tag = "test-server"
  #target = [module.target-group.target-id]
  client = "indigo"
  project = "indigo"
  environment = "prod"
  #sh = filebase64["${file("simplica-web.sh")}"]
  sh = filebase64("${path.module}/simplica-web.sh")
}

module "asg-1" {
  source ="./asg/"
  asg-1 = "test-1"
  key = "indigo-b2c-pp-asg"
  instance-type = "t2.micro"
  #security = [module.security.security-id]
  #subnet = [module.vpc.public-subnet-1]
  security = [data.aws_security_group.security_grp-1.id]
  subnet = [data.aws_subnet.Asg-subnet.id]
  template-1 = "test-2"
  ami = "ami-0230bd60aa48260c6"
  tag = "test-server-1"
  #target = [module.target-group.target-id]
  client = "indigo"
  project = "indigo"
  environment = "prod"
  sh = filebase64("${path.module}/hotels-all.sh")
}
*/
